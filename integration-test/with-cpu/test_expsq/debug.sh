#!/bin/bash

gem5_dir=${ALADDIN_HOME}/../..

${gem5_dir}/build/X86_MESI_Two_Level_aladdin/gem5.debug \
  --debug-flags=HybridDatapath,Aladdin,Exec \
  ${gem5_dir}/configs/aladdin/aladdin_se.py \
  $1
