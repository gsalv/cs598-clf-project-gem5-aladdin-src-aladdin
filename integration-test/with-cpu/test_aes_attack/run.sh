#!/bin/bash

cfg_home=${ALADDIN_HOME}/integration-test/with-cpu/test_aes_attack
bmk_home=${ALADDIN_HOME}/MachSuite/aes_attack/aes_attack
gem5_dir=${ALADDIN_HOME}/../..

#${gem5_dir}/build/X86/gem5.opt \
#  --debug-flags=HybridDatapath,Aladdin,AladdinTLB,ProtocolTrace,Exec \
#  --cpu-type=DerivO3CPU \

TRACE_VER=1
echo "cp dynamic_trace${TRACE_VER}.gz dynamic_trace.gz"
cp dynamic_trace${TRACE_VER}.gz dynamic_trace.gz

${gem5_dir}/build/X86_MESI_Two_Level_aladdin/gem5.debug \
  --outdir=${cfg_home}/outputs \
  --debug-flags=HybridDatapath,Aladdin,AladdinTLB,ProtocolTrace,Exec,TLB \
  ${gem5_dir}/configs/aladdin/aladdin_se.py \
  --ruby \
  --num-cpus=1 \
  --l1d_size=16kB \
  --l2_size=32kB \
  --mem-size=4GB \
  --mem-type=SimpleMemory  \
  --sys-clock=1GHz \
  --cpu-type=TimingSimpleCPU \
  --caches \
  --cacheline_size=64 \
  --accel_cfg_file=${cfg_home}/gem5.cfg \
  -c aes_attack \
  -o 'input.data check.data' \
| gzip -c > stdout.gz

#  --enable_prefetchers \
