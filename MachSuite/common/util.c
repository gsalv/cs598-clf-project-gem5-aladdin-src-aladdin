
#include "util.h"


/* Measure the time it takes to access a block with virtual address addr. */
//inline
CYCLES measure_one_block_access_time(ADDR_PTR addr)
{
  CYCLES cycles;

  asm volatile(
                    "mov %1, %%r8\n\t"
                    "lfence\n\t"
                    "rdtsc\n\t"
                    "mov %%eax, %%edi\n\t"
                    "mov (%%r8), %%r8\n\t"
                    "lfence\n\t"
                    "rdtsc\n\t"
                    "sub %%edi, %%eax\n\t"
                    : "=a"(cycles) /*output*/
                    : "r"(addr)
                    : "r8", "edi"
                );  

  return cycles;
}

//inline
CYCLES measure_one_set_access_time8(ADDR_PTR addr0, ADDR_PTR addr1, ADDR_PTR addr2, ADDR_PTR addr3, ADDR_PTR addr4, ADDR_PTR addr5, ADDR_PTR addr6, ADDR_PTR addr7)
{
  CYCLES cycles=0;
  //void* ret_addr0=0;
  //void* ret_addr1=0;
  //void* ret_addr2=0;
  //void* ret_addr3=0;
  //void* ret_addr4=0;
  //void* ret_addr5=0;
  //void* ret_addr6=0;
  //void* ret_addr7=0;

  //printf("addr0:%p addr1:%p addr2:%p addr3:%p addr4:%p addr5:%p addr6:%p addr7:%p\n", (void*)addr0, (void*)addr1, (void*)addr2, (void*)addr3, (void*)addr4, (void*)addr5, (void*)addr6, (void*)addr7);
  //printf("addr0:\n" );
  asm volatile(
                    //"mov %[addr0], %%r8\n\t"
                    //"mov %3, %%r8\n\t"
                    "lfence\n\t"
                    "rdtsc\n\t"
                    "mov %%eax, %%edi\n\t"

                    "mov %[in_addr0], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr0]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr1], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr1]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr2], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr2]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr3], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr3]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr4], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr4]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr5], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr5]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr6], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr6]\n\t"
                    //"lfence\n\t"
                    "mov %[in_addr7], %%r8\n\t"
                    "mov (%%r8), %%r8\n\t"//%[out_addr7]\n\t"

                    "lfence\n\t"
                    "rdtsc\n\t"
                    "sub %%edi, %%eax\n\t"
                    //: "=a"(cycles),"=r"(ret_addr0),"=r"(ret_addr1) /*output*/
                    : "=a"(cycles)//, [out_addr0]"=g"(ret_addr0),[out_addr1]"=g"(ret_addr1),[out_addr2]"=g"(ret_addr2),[out_addr3]"=g"(ret_addr3),[out_addr4]"=g"(ret_addr4),[out_addr5]"=g"(ret_addr5),[out_addr6]"=g"(ret_addr6),[out_addr7]"=g"(ret_addr7) /*output*/
                    //: "r"(addr0),"r"(addr1),"r"(addr2), "r"(addr3), "r"(addr4), "r"(addr5), "r"(addr6), "r"(addr7)
                    : [in_addr0]"g"(addr0),[in_addr1]"g"(addr1),[in_addr2]"g"(addr2), [in_addr3]"g"(addr3), [in_addr4]"g"(addr4), [in_addr5]"g"(addr5), [in_addr6]"g"(addr6), [in_addr7]"g"(addr7)
                    : "r8", "edi" 
                );  

  //printf("addr is:%d, %p, %p, %p, %p, %p, %p, %p, %p\n", cycles, ret_addr0, ret_addr1, ret_addr2, ret_addr3, ret_addr4, ret_addr5, ret_addr6, ret_addr7);
  return cycles;
}

//inline
CYCLES measure_one_set_access_time16(ADDR_PTR addr0, ADDR_PTR addr1, ADDR_PTR addr2, ADDR_PTR addr3, 
                                     ADDR_PTR addr4, ADDR_PTR addr5, ADDR_PTR addr6, ADDR_PTR addr7,
                                     ADDR_PTR addr8, ADDR_PTR addr9, ADDR_PTR addr10, ADDR_PTR addr11,
                                     ADDR_PTR addr12, ADDR_PTR addr13, ADDR_PTR addr14, ADDR_PTR addr15
)
{
  CYCLES cycles=0;

  //printf("addr0:%p addr1:%p addr2:%p addr3:%p addr4:%p addr5:%p addr6:%p addr7:%p\n", (void*)addr0, (void*)addr1, (void*)addr2, (void*)addr3, (void*)addr4, (void*)addr5, (void*)addr6, (void*)addr7);
  asm volatile(
                    //"mov %[addr0], %%r8\n\t"
                    //"mov %3, %%r8\n\t"
                    "lfence\n\t"
                    "rdtsc\n\t"
                    "mov %%eax, %%edi\n\t"

                    "mov %[in_addr0], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr0]\n\t"
                    "mov %[in_addr1], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr1]\n\t"
                    "mov %[in_addr2], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr2]\n\t"
                    "mov %[in_addr3], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr3]\n\t"
                    "mov %[in_addr4], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr4]\n\t"
                    "mov %[in_addr5], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr5]\n\t"
                    "mov %[in_addr6], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr6]\n\t"
                    "mov %[in_addr7], %%r8\n\t"
                    //"mov (%%r8), %%r8\n\t"//%[out_addr7]\n\t"
                    "mov %[in_addr8], %%r8\n\t"
                    "mov %[in_addr9], %%r8\n\t"
                    "mov %[in_addr10], %%r8\n\t"
                    "mov %[in_addr11], %%r8\n\t"
                    "mov %[in_addr12], %%r8\n\t"
                    "mov %[in_addr13], %%r8\n\t"
                    "mov %[in_addr14], %%r8\n\t"
                    "mov %[in_addr15], %%r8\n\t"

                    "lfence\n\t"
                    "rdtsc\n\t"
                    "sub %%edi, %%eax\n\t"
                    //: "=a"(cycles),"=r"(ret_addr0),"=r"(ret_addr1) /*output*/
                    : "=a"(cycles)
                    : [in_addr0]"g"(addr0),[in_addr1]"g"(addr1),[in_addr2]"g"(addr2), [in_addr3]"g"(addr3), [in_addr4]"g"(addr4), [in_addr5]"g"(addr5), [in_addr6]"g"(addr6), [in_addr7]"g"(addr7),[in_addr8]"g"(addr8),[in_addr9]"g"(addr9),[in_addr10]"g"(addr10), [in_addr11]"g"(addr11), [in_addr12]"g"(addr12), [in_addr13]"g"(addr13), [in_addr14]"g"(addr14), [in_addr15]"g"(addr15)
                    : "r8", "edi" 
                );  

  //printf("addr is:%d, %p, %p, %p, %p, %p, %p, %p, %p\n", cycles, ret_addr0, ret_addr1, ret_addr2, ret_addr3, ret_addr4, ret_addr5, ret_addr6, ret_addr7);
  return cycles;
}

VBYTE ** malloc_data()
{
  //static uint32_t x = 0x4938F912;
  int way;
  int data_size = ASSOCIATIVITY*NUM_L2_SETS*CACHE_LINE_SIZE;
  //int data_size = ASSOCIATIVITY*PAGE_SIZE_ATTACK;
  VBYTE ** data = (VBYTE **)malloc(ASSOCIATIVITY*sizeof(BYTE *));
  //VBYTE * data_temp = (VBYTE *)malloc(ASSOCIATIVITY*NUM_L2_SETS*CACHE_LINE_SIZE);
  VBYTE * data_temp = (VBYTE *)malloc(data_size+PAGE_SIZE_ATTACK);

  //const uintptr_t log_page_size = (uintptr_t)18;
  const uintptr_t log_page_size = (uintptr_t)12;
  data_temp = (VBYTE *)((((uintptr_t)data_temp) >> log_page_size) << log_page_size) + PAGE_SIZE_ATTACK;

  for (way = 0; way < ASSOCIATIVITY; way++) {
  //x ^= x << 13;
  //x ^= x >> 17;
  //x ^= x << 5;

  //const UINT random_val = x % 0x1000000;
      //uintptr_t data_temp = (uintptr_t)malloc(2*PAGE_SIZE_ATTACK*sizeof(BYTE) + random_val);

      data[way] = (VBYTE *)&data_temp[way*(data_size/ASSOCIATIVITY)*sizeof(BYTE)];
  }

  // Page-align data
  return data;
}


//inline
CYCLES prepare(VBYTE ** data, int set_idx)
{
    //CYCLES retval = 0;

    //for (int way = 0; way < ASSOCIATIVITY; way++) {
    //    data[way][set_idx*CACHE_LINE_SIZE] = 0;
    //}

    //for (int way = 0; way < ASSOCIATIVITY; way++) {
    //    retval += measure_one_block_access_time((ADDR_PTR)&(data[way][set_idx*CACHE_LINE_SIZE]));
    //}

    //return retval;

    flush(data, set_idx);
    return measure(data, set_idx);
}

//inline
int flush(VBYTE ** data, int set_idx) 
{
  return flush_ctrl(data, set_idx, 0, 0);
}

int flush_ctrl(VBYTE ** data, int set_idx, int start_way, int reverse)
{
    int ret =0;
    int iter, way;
    if (reverse) {
        for (iter = ASSOCIATIVITY; iter > 0; --iter) {
            way=(start_way+iter)%ASSOCIATIVITY;
            flush_way(data, set_idx, way);
        }
    } else {
        for (iter = 0; iter < ASSOCIATIVITY; ++iter) {
            way=(start_way+iter)%ASSOCIATIVITY;
            flush_way(data, set_idx, way);
        }
    }
    return ret;
}

//inline
int flush_way(VBYTE ** data, int set_idx, int way)
{
    data[way][set_idx*CACHE_LINE_SIZE]=0;
    return 0;
}


//inline
CYCLES measure(VBYTE ** data, int set_idx)
{
    float latency = 0;
    int i;
    for (i=0; i<SAMPLE_SIZE; i++) {
        //printf("addr0:%p addr1:%p addr2:%p addr3:%p addr4:%p\n", (void*)&data[0][set_idx*CACHE_LINE_SIZE], (void*)&data[1][set_idx*CACHE_LINE_SIZE], (void*)&data[2][set_idx*CACHE_LINE_SIZE], (void*)&data[3][set_idx*CACHE_LINE_SIZE], (void*)&data[4][set_idx*CACHE_LINE_SIZE]);
        latency += (float)measure_one_set_access_time8((ADDR_PTR)&data[0][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[1][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[2][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[3][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[4][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[5][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[6][set_idx*CACHE_LINE_SIZE],
                        (ADDR_PTR)&data[7][set_idx*CACHE_LINE_SIZE]);
    }

    return latency ;/// (SAMPLE_SIZE*ASSOCIATIVITY);
}

//inline
CYCLES measure_way(VBYTE ** data, int set_idx, int way)
{
    return measure_one_block_access_time((ADDR_PTR)&(data[way][set_idx*CACHE_LINE_SIZE]));
}

/*
void profile_access_times(RECORD &record)
{
    //int min_seen_1 = 0x0;
    //int min_seen_0 = 0x0;
    //int max_seen_1 = 0x0;
    //int max_seen_0 = 0x0;
    //for (int iter=0; iter<1000; iter++) {
    //    register int max_latency = 0;
    //    register int min_latency = 1000000000;
    //    register int total_latency = 0;
    //    register int max_latency_idx = 0;
    //    register int min_latency_idx = 0;

    //    for (int i=0; i<LINES_PER_PAGE; i++) {
    //        flush(record.data, i, 0);
    //    }
    //    for (int i=0; i<LINES_PER_PAGE; i++) {
    //        flush(record.data, i, 1);
    //        //flush(record.data, i, 1);
    //        //flush(record.data, i, 2);
    //    }
    //    for (int i=0; i<LINES_PER_PAGE; i++) {
    //        int latency = measure(record.data, i, 0);
    //        if (latency > max_latency) {
    //            max_latency = latency;
    //            max_latency_idx = i;
    //        }
    //        if (latency < min_latency) {
    //            min_latency = latency;
    //            min_latency_idx = i;
    //        }
    //        total_latency += latency;
    //    }
    //    min_seen_1 = min_seen_1 | min_latency_idx;
    //    min_seen_0 = min_seen_0 | ~min_latency_idx;
    //    max_seen_1 = max_seen_1 | max_latency_idx;
    //    max_seen_0 = max_seen_0 | ~max_latency_idx;
    //}
    //printf("min seen 1:0x%x\n", min_seen_1);
    //printf("min seen 0:0x%x\n", min_seen_0);
    //printf("max seen 1:0x%x\n", max_seen_1);
    //printf("max seen 0:0x%x\n", max_seen_0);
    //printf("max latency:%d\n", max_latency);
    //printf("max latency idx:0x%x\n", max_latency_idx);
    //printf("min latency:%d\n", min_latency);
    //printf("min latency idx:0x%x\n", min_latency_idx);
    //printf("avg latency:%f\n", (float)total_latency/(float)LINES_PER_PAGE);
    // Measure hit and miss times
    for (int i = 0; i < PROBES_PER_SET; ++i) {
        for (int j = 0; j < NUM_CHAR_LINES; ++j) {
            //for (int k = 0; k < ASSOCIATIVITY; ++k) {
                flush(record.data, j);
                //record.data[0][0]++;
                record.hit_times[j]  += (measure_one_set_access_time((ADDR_PTR)&record.data[0][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[1][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[2][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[3][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[4][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[5][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[6][j*CACHE_LINE_SIZE], 
                    (ADDR_PTR)&record.data[7][j*CACHE_LINE_SIZE]))/8;
            //}
            //for (int k = 0; k < ASSOCIATIVITY; ++k) {
                record.miss_times[j] += (measure_one_set_access_time((ADDR_PTR)&record.data_aux[0][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[1][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[2][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[3][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[4][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[5][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[6][j*CACHE_LINE_SIZE],
                    (ADDR_PTR)&record.data_aux[7][j*CACHE_LINE_SIZE])/8);
            //}   
        }
    }

    //for (int i = 0; i < PROBES_PER_SET; ++i) {
    //    for (int j = 0; j < NUM_CHAR_LINES; ++j) {
    //  int ways_visited = 0;
    //        for (int k = j%ASSOCIATIVITY; ways_visited < ASSOCIATIVITY; ++ways_visited, k = (k + 1)%ASSOCIATIVITY) {
    //            const CYCLES miss_time = measure(record.data_aux, record.permute_set[j], k);
    //            record.miss_times[k*NUM_CHAR_LINES + j] += (VFLOAT)miss_time;
    //        }
    //    }
    //}
    
    // Process access times
    for (int i = 0; i < NUM_CHAR_LINES; ++i) {
        int j = 0;
        //for (int j = 0; j < ASSOCIATIVITY; ++j) {
            const FLOAT norm_hit_time  = record.hit_times[j*NUM_CHAR_LINES + i] / ((FLOAT)PROBES_PER_SET);
            const FLOAT norm_miss_time = record.miss_times[j*NUM_CHAR_LINES + i] / ((FLOAT)PROBES_PER_SET);
    
            record.hit_times[j*NUM_CHAR_LINES + i] = norm_hit_time;
            record.avg_hit_time += norm_hit_time;
            if (record.min_hit_time > norm_hit_time)
                record.min_hit_time = norm_hit_time;
            if (record.max_hit_time < norm_hit_time)
                record.max_hit_time = norm_hit_time;
    
            record.miss_times[j*NUM_CHAR_LINES + i] = norm_miss_time;
            record.avg_miss_time += norm_miss_time;
            if (record.min_miss_time > norm_miss_time)
                record.min_miss_time = norm_miss_time;
            if (record.max_miss_time < norm_miss_time)
                record.max_miss_time = norm_miss_time;
    
            //if (i % 1000 == 0) {
                printf("norm_hit_time[%d][%d] : %0.03f\n", j, i, norm_hit_time);
                printf("norm_miss_time[%d][%d] : %0.03f\n", j, i, norm_miss_time);
            //}
        //}
        //if (i % 1000 == 0) {
            printf("---\n");
        //}
    }

    record.avg_hit_time = record.avg_hit_time / ((FLOAT)NUM_CHAR_LINES*ASSOCIATIVITY);
    record.avg_miss_time = record.avg_miss_time / ((FLOAT)NUM_CHAR_LINES*ASSOCIATIVITY);

    //// Print out statistics
    printf("Min hit time  : %0.03f\n", record.min_hit_time);
    printf("Max hit time  : %0.03f\n", record.max_hit_time);
    printf("Avg hit time  : %0.03f\n", record.avg_hit_time);
    printf("-------------------------\n");
    printf("Min miss time : %0.03f\n", record.min_miss_time);
    printf("Max miss time : %0.03f\n", record.max_miss_time);
    printf("Avg miss time : %0.03f\n", record.avg_miss_time);
}

void profile_access_times(unsigned int offset, RECORD &record)
{
    unsigned int j=offset;
    unsigned int idx=0;
    while (true) {
        for (int i=0; i<SAMPLE_SIZE; i++) {
            flush(record.data, j);
            //record.data[0][0]++;
            record.hit_times[j]  += (measure_one_set_access_time((ADDR_PTR)&record.data[0][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[1][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[2][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[3][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[4][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[5][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[6][j*CACHE_LINE_SIZE], 
                (ADDR_PTR)&record.data[7][j*CACHE_LINE_SIZE]))/8;

            record.miss_times[j] += (measure_one_set_access_time((ADDR_PTR)&record.data_aux[0][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[1][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[2][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[3][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[4][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[5][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[6][j*CACHE_LINE_SIZE],
                (ADDR_PTR)&record.data_aux[7][j*CACHE_LINE_SIZE])/8);
            idx++;
        }
        
        if (idx % SAMPLE_PRINT_SIZE == 0) {
            printf("avg hit:%f avg miss:%f\n", record.hit_times[j]/SAMPLE_SIZE, record.miss_times[j]/SAMPLE_SIZE);
            record.hit_times[j] = 0;
            record.miss_times[j] = 0;
        }
        record.hit_times[j] = 0; 
        record.miss_times[j] = 0; 
    }
}
*/

//bool receiver_waiting(VBYTE ** data, int iter) {
//    
//    float latency = measure(data, RCVR_WAITING_IDX);
//    if (iter % SAMPLE_PRINT_SIZE==0) {
//        printf("receiver_waiting latency:%f\n", latency);
//    }
//    return latency > LATENCY_THRESHOLD;
//}

void set_sending(VBYTE ** data) {
    flush(data, SNDR_SENDING_IDX);
}

void set_index(VBYTE ** data, int set_idx) {
    flush(data, set_idx);
}

//bool sender_sending(VBYTE ** data) {
//    
//    float latency = measure(data, SNDR_SENDING_IDX);
//    return latency > LATENCY_THRESHOLD;
//}

void set_waiting(VBYTE ** data) {
    flush(data, RCVR_WAITING_IDX);
}

BYTE get_sent_index(VBYTE ** data) {
    BYTE max_idx = 0xff;
    int idx;
    float latency = measure(data, SNDR_SENDING_IDX);
    if (latency > LATENCY_THRESHOLD) {
        float max_latency = 0;
        for (idx = 0; idx < NUM_CHAR_LINES; idx++) {
            set_waiting(data);
            latency = measure(data, idx);
            if (latency > max_latency && latency > LATENCY_THRESHOLD) {
                max_latency = latency;
                max_idx = idx;
            }
        }
    }
    return max_idx;
}


inline uintptr_t align_mem(uintptr_t addr, size_t alignment)
{
	const size_t mask = alignment - 1;
    return (uintptr_t) ((addr + mask) & ~mask);
}

//inline volatile uintptr_t align_mem(volatile uintptr_t addr, size_t alignment)
//{
//	const size_t mask = alignment - 1;
//    return (volatile uintptr_t) ((addr + mask) & ~mask);
//}
