#ifndef __UTIL_HPP__
#define __UTIL_HPP__

// You may only use fgets() to pull input from stdin
// You may use any print function to stdout to print 
// out chat messages
#include <stdio.h>
//#include <iostream>

// You may use memory allocators and helper functions 
// (e.g., rand()).  You may not use system().
#include <stdlib.h>

#include <inttypes.h>
#include <time.h>
#include <math.h>


// FIXME: For debugging. Remove for submission.
#ifdef DEBUG
#include <assert.h>
#define my_assert(condition) (assert((condition)))
#else
#define my_assert(condition)
#endif

#ifndef PAGE_SIZE
#define PAGE_SIZE 4096
#endif


#define ADDR_PTR uint64_t 
#define CYCLES   uint32_t
#define BYTE     uint8_t
#define INT      int32_t
#define UINT     uint32_t
#define FLOAT    float
#define VBYTE    volatile uint8_t
#define VINT     volatile int32_t
#define VUINT    volatile uint32_t
#define VFLOAT   volatile float

#define CACHE_LINE_SIZE     64          // Bytes
#define ASSOCIATIVITY       8
#define L2_SIZE             0x10000      // 64K    // 2 MB
#define NUM_L2_SETS         L2_SIZE/(ASSOCIATIVITY*CACHE_LINE_SIZE)
#define PAGE_SIZE_ATTACK           0x2000        // Bytes
//#define PAGE_SIZE_ATTACK           0x200000    // Bytes
//#define PAGE_SIZE_ATTACK           (512*(ASSOCIATIVITY)*(CACHE_LINE_SIZE))    // Bytes
#define LINES_PER_PAGE      ((PAGE_SIZE_ATTACK)/(CACHE_LINE_SIZE))

#define PROBES_PER_SET      1
#define NUM_CHAR_LINES      16
#define SAMPLE_SIZE         1
#define SAMPLE_PRINT_SIZE   10000000
//#define NUM_CHAR_LINES       PAGE_SIZE_ATTACK/(2*CACHE_LINE_SIZE)
//#define NUM_CHAR_LINES      16
//#define NUM_CHAR_LINES      256

#define CHAR_LINES_OFFSET   ((ADDR_PTR)0x0)
//#define CHAR_LINES_OFFSET   ((ADDR_PTR)0x10)
#define HIT_TIMES_OFFSET    ((ADDR_PTR)((CHAR_LINES_OFFSET) + (CACHE_LINE_SIZE)*(NUM_CHAR_LINES)*sizeof(VBYTE)))
#define MISS_TIMES_OFFSET   ((ADDR_PTR)((HIT_TIMES_OFFSET) + (ASSOCIATIVITY)*(NUM_CHAR_LINES)*sizeof(VFLOAT)))
//#define RECORD_OFFSET       ((ADDR_PTR)((MISS_TIMES_OFFSET) + ()))
#define PERMUTE_SET_OFFSET  ((ADDR_PTR)((MISS_TIMES_OFFSET) + (ASSOCIATIVITY)*(NUM_CHAR_LINES)*sizeof(VFLOAT)))

#define RCVR_WAITING_IDX NUM_CHAR_LINES
#define SNDR_SENDING_IDX RCVR_WAITING_IDX+1
#define LATENCY_THRESHOLD 8.0

/*
struct RECORD
{
    VBYTE ** data     = NULL;
    VBYTE ** data_aux = NULL;

    INT      num_probes;
    VBYTE  * char_lines;

    VFLOAT * hit_times;
    VFLOAT   min_hit_time;
    VFLOAT   max_hit_time;
    VFLOAT   avg_hit_time;

    VFLOAT * miss_times;
    VFLOAT   min_miss_time;
    VFLOAT   max_miss_time;
    VFLOAT   avg_miss_time;

    //int * permute_set;

    RECORD() : data(NULL),
			   data_aux(NULL),
			   num_probes(0),
			   min_hit_time(INFINITY),
			   max_hit_time(0.0f),
			   avg_hit_time(0.0f),
			   min_miss_time(INFINITY),
			   max_miss_time(0.0f),
			   avg_miss_time(0.0f)
	{
		// Do nothing
		printf("MAKE SURE TO INITIALIZE DATA BUFFERS!\n");
	}
    RECORD(VBYTE ** _data, VBYTE ** _data_aux) : data(_data),
                                                 data_aux(_data_aux),
                                                 num_probes(0),
                                                 min_hit_time(INFINITY),
                                                 max_hit_time(0.0f),
                                                 avg_hit_time(0.0f),
                                                 min_miss_time(INFINITY),
                                                 max_miss_time(0.0f),
                                                 avg_miss_time(0.0f)
    {
        my_assert(data     != NULL);
        my_assert(data_aux != NULL);

        char_lines = (VBYTE *)&data[0][CHAR_LINES_OFFSET];
        hit_times  = (VFLOAT *)&data[0][HIT_TIMES_OFFSET];
        miss_times = (VFLOAT *)&data[0][MISS_TIMES_OFFSET];
        //permute_set = (INT *)&data[0][PERMUTE_SET_OFFSET];

        for (int j = 0; j < ASSOCIATIVITY; ++j) {
            for (int i = 0; i < NUM_CHAR_LINES; ++i) {
                hit_times[j*NUM_CHAR_LINES + i]  = 0.0f;
                miss_times[j*NUM_CHAR_LINES + i] = 0.0f;
            }
        }

        //permute_set[0] = 7;
        //permute_set[1] = 1;
        //permute_set[2] = 2;
        //permute_set[3] = 8;
        //permute_set[4] = 11;
        //permute_set[5] = 15;
        //permute_set[6] = 6;
        //permute_set[7] = 3;
        //permute_set[8] = 4;
        //permute_set[9] = 0;
        //permute_set[10] = 9;
        //permute_set[11] = 13;
        //permute_set[12] = 5;
        //permute_set[13] = 14;
        //permute_set[14] = 10;
        //permute_set[15] = 12;
    }

	void print_addresses()
	{
		printf("char_lines : %p\n", (void *)char_lines);
		printf("hit_times : %p\n", (void *)hit_times);
		printf("miss_times : %p\n", (void *)miss_times);
		//printf("permute_set : %p\n", (void *)permute_set);
		printf("&char_lines : %p\n", (void *)&char_lines);
		printf("&hit_times : %p\n", (void *)&hit_times);
		printf("&miss_times : %p\n", (void *)&miss_times);
		//printf("&permute_set : %p\n", (void *)&permute_set);
	}
};
*/

CYCLES measure_one_block_access_time(ADDR_PTR addr);

CYCLES measure_one_set_access_time8(ADDR_PTR addr0,
                                   ADDR_PTR addr1,
                                   ADDR_PTR addr2,
                                   ADDR_PTR addr3,
                                   ADDR_PTR addr4,
                                   ADDR_PTR addr5,
                                   ADDR_PTR addr6,
                                   ADDR_PTR addr7);
CYCLES measure_one_set_access_time16(ADDR_PTR addr0,
                                   ADDR_PTR addr1,
                                   ADDR_PTR addr2,
                                   ADDR_PTR addr3,
                                   ADDR_PTR addr4,
                                   ADDR_PTR addr5,
                                   ADDR_PTR addr6,
                                   ADDR_PTR addr7,
                                   ADDR_PTR addr8,
                                   ADDR_PTR addr9,
                                   ADDR_PTR addr10,
                                   ADDR_PTR addr11,
                                   ADDR_PTR addr12,
                                   ADDR_PTR addr13,
                                   ADDR_PTR addr14,
                                   ADDR_PTR addr15);

VBYTE ** malloc_data();

CYCLES prepare(VBYTE ** data, int set_idx);

int flush(VBYTE ** data, int set_idx);
int flush_way(VBYTE ** data, int set_idx, int way);
int flush_ctrl(VBYTE ** data, int set_idx, int start_way, int reverse);

CYCLES measure(VBYTE ** data, int set_idx);
CYCLES measure_way(VBYTE ** data, int set_idx, int way);

//void profile_access_times(RECORD &record);
//void profile_access_times(unsigned int offset, RECORD &record);

//bool receiver_waiting(VBYTE ** data, int iter);
void set_sending(VBYTE ** data);
void set_index(VBYTE ** data, int set_idx);

//bool sender_sending(VBYTE ** data);
void set_waiting(VBYTE ** data);
BYTE get_sent_index(VBYTE ** data);

uintptr_t align_mem(uintptr_t addr, size_t alignment);

#endif //__UTIL_HPP__
