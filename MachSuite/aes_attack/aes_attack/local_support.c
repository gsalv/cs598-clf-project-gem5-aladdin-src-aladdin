#include "aes_attack.h"
#include <string.h>
#include "../../../gem5/aladdin_sys_constants.h"

#ifdef GEM5_HARNESS
#include "gem5/gem5_harness.h"
#endif

//extern uint8_t sbox[];
extern unsigned long sbox[];
//extern int NOT_COMPLETED;
//extern unsigned long sbox_aligned_temp[];
#define NUM_ATTACK_ARRAYS 1
#define INTER_PHASE_DELAY 1000

int INPUT_SIZE = sizeof(struct bench_args_t)+PAGE_SIZE;

void call_accel(struct bench_args_t *args) {
#ifdef GEM5_HARNESS
  mapArrayToAccelerator(
      MACHSUITE_AES_AES, "ctx", (void*)&args->ctx, sizeof(args->ctx));
  mapArrayToAccelerator(
      MACHSUITE_AES_AES, "k", (void*)&args->k, sizeof(args->k));
  mapArrayToAccelerator(
      MACHSUITE_AES_AES, "buf", (void*)&args->buf, sizeof(args->buf));
  mapArrayToAccelerator(
      //MACHSUITE_AES_AES, "sbox_aligned", (void*)sbox_aligned, 256*sizeof(uint8_t));
      MACHSUITE_AES_AES, "sbox_aligned", (void*)sbox_aligned, 256*sizeof(unsigned long));
  invokeAcceleratorAndBlock(MACHSUITE_AES_AES);
#else
  aes256_encrypt_ecb( &(args->ctx), args->k, args->buf, sbox_aligned, INTER_PHASE_DELAY );
#endif
}

int* call_accel_asynch(struct bench_args_t *args) {
#ifdef GEM5_HARNESS
  mapArrayToAccelerator(
      MACHSUITE_AES_AES, "ctx", (void*)&args->ctx, sizeof(args->ctx));
  mapArrayToAccelerator(
      MACHSUITE_AES_AES, "k", (void*)&args->k, sizeof(args->k));
  mapArrayToAccelerator(
      MACHSUITE_AES_AES, "buf", (void*)&args->buf, sizeof(args->buf));
  mapArrayToAccelerator(
      //MACHSUITE_AES_AES, "sbox_aligned", (void*)sbox_aligned, 256*sizeof(uint8_t));
      MACHSUITE_AES_AES, "sbox_aligned", (void*)sbox_aligned, 256*sizeof(unsigned long));
  return invokeAcceleratorAndReturn(MACHSUITE_AES_AES);
#else
  aes256_encrypt_ecb( &(args->ctx), args->k, args->buf, sbox_aligned, INTER_PHASE_DELAY );
  return 0;
#endif
}

void run_benchmark( void *vargs ) {
  struct bench_args_t *args = (struct bench_args_t *)vargs;

  // setup aligned sbox, attacker array
  int i,j,attempt;
  volatile int* done_ptr;
  const uintptr_t log_page_size = (uintptr_t)12;
  //uint8_t* sbox_aligned_temp = (uint8_t*)malloc(256*sizeof(uint8_t) + PAGE_SIZE);
  int margin = CACHE_LINE_SIZE;
  unsigned long* sbox_aligned_temp = (unsigned long*)malloc(256*sizeof(uint8_t) + 3*PAGE_SIZE + margin);
  //sbox_aligned = (uint8_t *)((((uintptr_t)sbox_aligned_temp) >> log_page_size) << log_page_size) + PAGE_SIZE_ATTACK;
  sbox_aligned = (unsigned long *)(((((uintptr_t)sbox_aligned_temp) >> log_page_size) << log_page_size) + PAGE_SIZE);
  
  for (i=0; i<256; i++) {
    sbox_aligned[i] = sbox[i];
  } 
  //sbox_aligned = sbox;

  printf("sbox_aligned addr:%p\n", sbox_aligned);

#if defined(USE_SAME_ADDR) || defined(ASYNCHRONOUS_ATTACK)
  // setup attack arrays- need multiple because we don't know what will map to correct L2 banks
  VBYTE ** attack_data_array[NUM_ATTACK_ARRAYS];
  //VBYTE *** attack_data_array = (VBYTE***)malloc(num_attack_arrays*sizeof(VBYTE**));
  for (i=0; i<NUM_ATTACK_ARRAYS; i++) {
    attack_data_array[i] = malloc_data();
    printf("array %d allocated at [", i);
    for (j=0; j<ASSOCIATIVITY; j++) {
      printf("%p, ", (void*)attack_data_array[i][j]);
    }
    printf("]\n");
  }
#else
  VBYTE ** attack_data_array = malloc_data();
  printf("array %d allocated at [", 0);
  for (i = 0; i < ASSOCIATIVITY; i++) {
    printf("%p%s ", &(attack_data_array[i][0]), ((i < ASSOCIATIVITY - 1) ? "," : ""));
  }
  printf("]\n");
#endif

  // keep track of latency differences
  //CYCLES * last_latency = (CYCLES*)malloc(NUM_L2_SETS*sizeof(CYCLES));
#ifdef USE_SAME_ADDR
#ifdef ASYNCHRONOUS_ATTACK
  int print_granularity = 64;
  CYCLES ** last_latency = (CYCLES**)malloc(print_granularity*sizeof(CYCLES*));
  for (i=0; i<print_granularity; i++) {
    last_latency[i] = (CYCLES*)malloc((256/8)*sizeof(CYCLES));
  }
#else
  CYCLES * last_latency = (CYCLES*)malloc(LINES_PER_PAGE*sizeof(CYCLES));
#endif
#else
  CYCLES ** latencies = (CYCLES **)malloc(32*sizeof(CYCLES *));
  for (i = 0; i < 32; i++) {
    CYCLES * temp = (CYCLES *)malloc(8*sizeof(CYCLES));
    latencies[i] = temp;
  }
#endif

  int warmup=0;
  for (attempt=0; attempt<NUM_ATTACK_ARRAYS; attempt++) {
#ifndef USE_SAME_ADDR
    int max_diff = 0;
    int max_latency = 0;
    int min_diff = 0;
    int max_offset = -1;
    int min_offset = -1;
#endif

#if defined(USE_SAME_ADDR) || defined(ASYNCHRONOUS_ATTACK)
    // bring target data into cache / flush any sbox data
    //for (i=0; i<LINES_PER_PAGE; i++) {
    for (i=0; i<256/8; i++) {
      //for (j=0; j<8; j++) {
      //  flush_ctrl(attack_data_array[attempt], i, j, j&1);
      //}
      flush_ctrl(attack_data_array[attempt], i, 0, 0);
      flush_ctrl(attack_data_array[attempt], i, 0, 1);
      flush_ctrl(attack_data_array[attempt], i, 4, 0);
    }
#else
    // bring target data into cache / flush any sbox data
    //for (i=0; i<LINES_PER_PAGE; i++) {
    for (i = 0; i < 256/8; i++) {
      //for (j=0; j<8; j++) {
      //  flush_ctrl(attack_data_array[attempt], i, j, j&1);
      //}
      flush_ctrl(attack_data_array, i, 0, 0);
      flush_ctrl(attack_data_array, i, 0, 1);
      flush_ctrl(attack_data_array, i, 4, 0);
    }
#endif

//#ifndef USE_SAME_ADDR
//    // measure hit latency
//    for (i=0; i<LINES_PER_PAGE; i++) {
//      //printf("before measure %d\n", i);
//      int temp  = measure(attack_data_array[attempt], i);
//      //printf("after measure %d got %d\n", i, temp);
//      last_latency[i]  = temp;
//    }
//    // warmup so measure insts are in icache
    int inst_warmup = 0;
    measure_one_block_access_time((ADDR_PTR)&inst_warmup);
//#endif
#ifdef ASYNCHRONOUS_ATTACK
    // launch accelerator
    if (attempt >= warmup) {
      done_ptr = call_accel_asynch(args);
      //printf("after call accel\n");
    }
#else
    // launch accelerator
    if (attempt >= warmup) {
      call_accel(args);
    }
#endif

#ifdef GEM5_HARNESS
#ifdef USE_SAME_ADDR
#ifdef ASYNCHRONOUS_ATTACK
    int logged=0;
    int printed=0;
    int flagval = *done_ptr;
    //printf("Read %p val=%x\n", (void*)done_ptr, flagval);
    while (*done_ptr==NOT_COMPLETED) {
      for (i=0; i<print_granularity && *done_ptr==NOT_COMPLETED; i++) {
        // check if sbox has been loaded into cache by accelerator
        for (j=0; j<(256/8); j++) {
          int temp  = measure_one_block_access_time((ADDR_PTR)&sbox_aligned[j*8]);
          //printf("after measure addr:%p got latency[%d]=%d\n", (void*)&sbox_aligned[j], j, temp);
          last_latency[i][j]  = temp;
        }
        // try to flush sbox out of cache
        for (j=0; j<256/8; j++) {
          //for (j=0; j<8; j++) {
            //printf("DEBUG iter %d before flush %d 0\n", i, j);
            flush_ctrl(attack_data_array[attempt], j, 0, 0);
            //printf("DEBUG iter %d before flush %d 1\n", i, j);
            //flush_ctrl(attack_data_array[attempt], j, 0, 1);
          //}
        }
        logged++;
      }
      
      for (; printed<logged; printed++) {
        printf("# latencies try %d, ", printed % print_granularity);
        for (i=0; i<(256/8); i++) {
          printf("%d, ", last_latency[printed % print_granularity][i]);
        }
        printf("\n");
      }
      flagval = *done_ptr;
      //printf("Read2 %p %d val=%x\n", (void*)done_ptr, flagval);
    }
#else
    for (i=0; i<256; i+=8) {
      int temp  = measure_one_block_access_time((ADDR_PTR)&sbox_aligned[i]);
      //printf("after measure addr:%p got latency[%d]=%d\n", (void*)&sbox_aligned[i], i, temp);
      last_latency[i]  = temp;
    }
    printf("# latencies, ");
    for (i=0; i<256; i+=8) {
      //printf("after measure addr:%p latency[%d]=%d\n", (void*)&sbox_aligned[i], i, last_latency[i]);
      printf("%d, ", last_latency[i]);
    }
    printf("\n");
#endif
#else
    for (i = 0; i < 32; i++) {
      for (int way = 0; way < ASSOCIATIVITY; way++) {
        CYCLES latency = measure_one_block_access_time((ADDR_PTR)&(attack_data_array[way][i*CACHE_LINE_SIZE]));
        latencies[i][way] = latency;
      }
    }

    for (i = 0; i < 32; i++) {
      bool a_miss = false;
      printf("###latencies[%d],", i);
      for (int way = 0; way < ASSOCIATIVITY; way++) {
        CYCLES latency = latencies[i][way];
        printf("%03d%s", latency, ((way < ASSOCIATIVITY - 1) ? "," : ""));
        a_miss = a_miss || (latency > 40);
      }
      printf(",\"\t-> %d\"\n", (int)a_miss);
    }

    //// compare post-accel latencies with hit latencies
    //for (i=0; i<LINES_PER_PAGE; i++) {
    //  //printf("before measure2 %d\n", i);
    //  int latency = (int)measure(attack_data_array[attempt], i);
    //  int latency_diff = latency - (int)last_latency[i];
    //  printf("after measure2 %d got %d (diff=%d)\n", i, latency, latency_diff);
    //  if (latency_diff > max_diff) {
    //    max_diff = latency_diff;
    //    max_latency = latency;
    //    max_offset = i;
    //  }
    //  if (latency_diff < min_diff) {
    //    min_diff = latency_diff;
    //    min_offset = i;
    //  }
    //}
    //printf("attack array %d has max latency diff %d (%d) at offset %d\n", attempt, max_diff, max_latency, max_offset);
    //printf("attack array %d has min latency diff %d at offset %d\n", attempt, min_diff, min_offset);
#endif
#endif
  }
  printf("done.\n");
}

/* Input format:
%%: Section 1
uint8_t[32]: key
%%: Section 2
uint8_t[16]: input-text
*/

void input_to_data(int fd, void *vdata) {
  struct bench_args_t *data = (struct bench_args_t *)vdata;
  char *p, *s;
  // Zero-out everything.
  memset(vdata,0,sizeof(struct bench_args_t));
  // Load input string
  p = readfile(fd);
  // Section 1: key
  s = find_section_start(p,1);
  parse_uint8_t_array(s, data->k, 32);
  // Section 2: input-text
  s = find_section_start(p,2);
  parse_uint8_t_array(s, data->buf, 16);
}

void data_to_input(int fd, void *vdata) {
  struct bench_args_t *data = (struct bench_args_t *)vdata;
  // Section 1
  write_section_header(fd);
  write_uint8_t_array(fd, data->k, 32);
  // Section 2
  write_section_header(fd);
  write_uint8_t_array(fd, data->buf, 16);
}

/* Output format:
%% Section 1
uint8_t[16]: output-text
*/

void output_to_data(int fd, void *vdata) {
  struct bench_args_t *data = (struct bench_args_t *)vdata;

  char *p, *s;
  // Zero-out everything.
  memset(vdata,0,sizeof(struct bench_args_t));
  // Load input string
  p = readfile(fd);
  // Section 1: output-text
  s = find_section_start(p,1);
  parse_uint8_t_array(s, data->buf, 16);
}

void data_to_output(int fd, void *vdata) {
  struct bench_args_t *data = (struct bench_args_t *)vdata;
  // Section 1
  write_section_header(fd);
  write_uint8_t_array(fd, data->buf, 16);
}

int check_data( void *vdata, void *vref ) {
  struct bench_args_t *data = (struct bench_args_t *)vdata;
  struct bench_args_t *ref = (struct bench_args_t *)vref;
  int has_errors = 0;

  // Exact compare encrypted output buffers
  has_errors |= memcmp(&data->buf, &ref->buf, 16*sizeof(uint8_t));

  // Return true if it's correct.
  return !has_errors;
}
