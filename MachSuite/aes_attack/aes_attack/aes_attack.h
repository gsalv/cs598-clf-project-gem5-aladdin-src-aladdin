/*
*   Byte-oriented AES-256 implementation.
*   All lookup tables replaced with 'on the fly' calculations.
*/
#include "support.h"
#include "util.h"

#ifndef AES_ATTACK_H
#define AES_ATTACK_H

//#define USE_SAME_ADDR

#ifndef PAGE_SIZE
#define PAGE_SIZE 4096
#endif

//#define ASYNCHRONOUS_ATTACK
//uint8_t *sbox_aligned;
unsigned long *sbox_aligned;

typedef struct {
  uint8_t key[32];
  uint8_t enckey[32];
  uint8_t deckey[32];
} aes256_context;

void aes256_encrypt_ecb(aes256_context *ctx, uint8_t k[32], uint8_t buf[16], unsigned long *sbox_aligned, int inter_phase_delay);

////////////////////////////////////////////////////////////////////////////////
// Test harness interface code.

struct bench_args_t {
  aes256_context ctx;
  uint8_t k[32];
  uint8_t buf[16];
};


#endif
